﻿using UnityEngine;
using System.Collections;

public class Script_PlayerHealth : MonoBehaviour
{
    public int health = 100;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > 2f)
        {
            Debug.Log(health);
        }
    }

    public void TakeDamage()
    {
        health -= 25;
    }

    public void TakeHealing()
    {
        health += 25;
    }
}
